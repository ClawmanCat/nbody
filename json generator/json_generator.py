# Copyright (c) Rob Klein Ikink, 2018. Licensed under CC-BY 4.0
# http://creativecommons.org/licenses/by/4.0/deed.en_US

import telnetlib
import sys
import re
import time
import json

HOST = "horizons.jpl.nasa.gov"
PORT = 6775
DEST = "uitvoer.json"

INTERVAL_READ    = 0.5
INTERVAL_CONNECT = 3.0

# Niet bestaande string zodat de client doorleest tot het EOF
FAKESTRING = "Skadiedel Skadoedel, uw kop lijkt op een poedel."

def sendCommand(tn, str):
    tn.read_until(FAKESTRING, INTERVAL_READ)
    tn.write(str + "\r\n")
    time.sleep(INTERVAL_READ)
    return

def findPairFirstElement(array, elem):
    for element in array:
        if element[0] == elem:
            return element;

    return None

def selectBodiesFromDefaultState(exitAfterOne, exitString):
    returnValues = []
    uInput = raw_input("Voer de naam van een lichaam in: ")
    firstRun = True

    while not (exitAfterOne and not firstRun) and uInput.lower() != exitString.lower():
        sendCommand(tn, uInput)
        output = tn.read_until(FAKESTRING, INTERVAL_READ)

        if re.search(r"Multiple\smajor-bodies", output, re.X) is not None:
            # Meerdere resultaten

            print("Er zijn meerdere resultaten voor de naam \"" + uInput + "\":")

            results = []
            for result in re.finditer(r'([0-9]+)\s\s(([A-Za-z\-\(\)0-9]+|\s\S)+).+$', output, re.X | re.M):
                print(result.group(1).ljust(6) + " -- " + result.group(2))
                results.append([result.group(1), result.group(2)])

            selection = raw_input("Voer het gewenste nummer in: ")
            
            while findPairFirstElement(results, selection) is None:
                selection = raw_input("\"" + selection + "\" is niet gevonden. Probeer opnieuw: ")

            body = findPairFirstElement(results, selection)
            print("Het lichaam " + body[1] + " (" + body[0] + ") is geselecteerd.")
            returnValues.append(body)
        elif re.search(r'\>EXACT\<', output, re.X) is None:
            # Een resultaat

            result = re.search(r'Revised.*(' + re.escape(uInput) + ')[^0-9]*([0-9]+).*$', output, re.M | re.I | re.X)

            body = [result.group(2), result.group(1)]
            print("Het lichaam " + body[1] + " (" + body[0] + ") is geselecteerd.")
            returnValues.append(body)
        else:
            # Geen resultaten

            print("Geen resultaten gevonden.")
            
            sendCommand(tn, "n")
            time.sleep(INTERVAL_READ)
            

        firstRun = False
        if not (exitAfterOne and not firstRun):
            uInput = raw_input("Voer de naam van een lichaam in: ")
    
    return returnValues


# Begin programma

# Maak verbinding met de Telnet server
print("Verbinden met server...")

tn = telnetlib.Telnet(HOST, PORT)
time.sleep(INTERVAL_CONNECT)

if tn is None:
    raise Exception("Kan geen verbinding maken met server.")

print("Verbonden met server.")


# Verkrijg de gewenste lichamen
print("Voer de naam van een lichaam in om data te verzamelen. Voer \"gereed\" in als u klaar bent.")
print("Typ voor het beste resultaat de gewenste naam in het Engels.")

bodies = selectBodiesFromDefaultState(False, "gereed")

print("De volgende lichamen zijn geselecteerd:")
for body in bodies:
    print(body[1] + " (" + body[0] + ")")


# Verkrijg de ouder van elk lichaam
bodiesWithParent = []

for body in bodies:
    print("Voer de naam in van de ouder van " + body[0] + " (" + body[1] + ")")
    print("(De ouder is het lichaam waar dit lichaam omheen draait.)")
    print("Voer \"OORSPRONG\" in als dit lichaam het centrum van het model vormt.")

    parent = selectBodiesFromDefaultState(True, "oorsprong")
    if parent is None:
        print(name[0] + " (" + name[1] + ") is nu de oorsprong van het model.")

    bodiesWithParent.append([body, parent])


# Verkrijg de parameters van de lichamen
prev = False
data = {}

print("Data wordt opgehaald. Dit kan enkele minuten duren...")

for body in bodiesWithParent:
    # Verbind opnieuw om te voorkomen dat er een nieuwsbericht verschijnt
    tn.close();
    tn = telnetlib.Telnet(HOST, PORT)
    time.sleep(INTERVAL_CONNECT)
    
    childID    = body[0][0]
    childName  = body[0][1]

    parentID   = -999999
    parentName = "UNDEFINED"

    if len(body[1]) != 0:
        parentID   = body[1][0][0]
        parentName = body[1][0][1]


    # Selecteer het lichaam
    sendCommand(tn, childID)

    # Verkrijg massa en straal
    geodata = tn.read_until(FAKESTRING, INTERVAL_READ)

    # Gebruikte syntax is inconsistent    
    masscomponent_format_a = re.search(r'Mass\s*\(10\^([0-9]+)\s*kg\s*\)\s*[=|~]\s*([0-9\.]+)', geodata, re.X)
    masscomponent_format_b = re.search(r'Mass,\s*10\^([0-9]+)\s*kg\s*[=|~]\s*([0-9\.]+)', geodata, re.X)
    masscomponent_format_c = re.search(r'Mass\s*' + re.escape(childName) + '\s*\(10\^([0-9]+)\s*kg\s*\)\s*[=|~]\s*([0-9\.]+)', geodata, re.X)
    
    masscomponent = 0
    if masscomponent_format_a is not None:
        masscomponent = masscomponent_format_a
    elif masscomponent_format_b is not None:
        masscomponent = masscomponent_format_b
    else:
        masscomponent = masscomponent_format_c

    # Verkrijg de mantissa en het exponent en bouw daaruit het getal op
    exponent = masscomponent.group(1)
    mantissa = masscomponent.group(2)

    mass = mantissa + "e" + exponent

    # Syntax is ook hier inconsistent, maar deze regex vangt allebei
    radius = re.search(r'[R|r]adius[^\.]+=\s*([0-9\.]+)', geodata, re.X).group(1)


    # Verkrijg cartesiaanse vectors
    px, py, pz, vx, vy, vz = 0, 0, 0, 0, 0, 0
    
    if parentName != "UNDEFINED":
        # Selecteer "Ephemeris"
        sendCommand(tn, "e")
        # Selecteer "Vectors"
        sendCommand(tn, "v")

        # Redefinitie centrum
        # if (prev):
        #    sendCommand(tn, "n")
        
        # Coordinate center = parent
        sendCommand(tn, "@" + parentID)
        # Relatief aan de ecliptica
        sendCommand(tn, "eclip")

        # Begin- en einddatum
        sendCommand(tn, "2000AD-Jan-01")
        sendCommand(tn, "2000AD-Jan-02")
        sendCommand(tn, "1d")

        # Geen standaardoutput (Wij willen km/s, niet AU/d)
        sendCommand(tn, "n")

        # Output Parameters
        sendCommand(tn, "J2000")     # J2000 Ecliptica
        sendCommand(tn, "1")         # Geen correctie
        sendCommand(tn, "1")         # km/s
        sendCommand(tn, "n")         # CSV
        sendCommand(tn, "n")         # Geen Delta T
        sendCommand(tn, "2")         # Positie en snelheid
        sendCommand(tn, "y")         # Label


        # Lees de ontstane data en verkrijg daaruit de vectors
        vectorData = tn.read_until(FAKESTRING, INTERVAL_READ)

        px = re.search(r'X\s*=\s*(\-?[0-9]+(\.[0-9]+)?E(\+|\-)[0-9]+)', vectorData, re.X).group(1)
        py = re.search(r'Y\s*=\s*(\-?[0-9]+(\.[0-9]+)?E(\+|\-)[0-9]+)', vectorData, re.X).group(1)
        pz = re.search(r'Z\s*=\s*(\-?[0-9]+(\.[0-9]+)?E(\+|\-)[0-9]+)', vectorData, re.X).group(1)

        vx = re.search(r'VX\s*=\s*(\-?[0-9]+(\.[0-9]+)?E(\+|\-)[0-9]+)', vectorData, re.X).group(1)
        vy = re.search(r'VY\s*=\s*(\-?[0-9]+(\.[0-9]+)?E(\+|\-)[0-9]+)', vectorData, re.X).group(1)
        vz = re.search(r'VZ\s*=\s*(\-?[0-9]+(\.[0-9]+)?E(\+|\-)[0-9]+)', vectorData, re.X).group(1)

    data[childName] = []
    data[childName].append({
        "locatie_crt": {
            "px": float(px) * 1000,
            "py": float(py) * 1000,
            "pz": float(pz) * 1000,

            "vx": float(vx) * 1000,
            "vy": float(vy) * 1000,
            "vz": float(vz) * 1000
        },

        "massa":  mass,
        "straal": float(radius) * 1000,
    })

    if parentName != "UNDEFINED":
        data[childName].append({
            "ouder": parentName
        })
    else:
        data[childName].append({
            "ouder": "OORSPRONG"
        })

    prev = True

    
    # Print de verkregen data
    print("Data voor lichaam " + str(childName) + " (" + str(childID) + "):")
    print("Positievector:   [" + str(float(px) * 1000) + ", " + str(float(py) * 1000) + ", " + str(float(pz) * 1000) + "] m")
    print("Snelheidsvector: [" + str(float(vx) * 1000) + ", " + str(float(vy) * 1000) + ", " + str(float(vz) * 1000) + "] m/s")
    print("Massa:  " + str(mass) + " kg")
    print("Straal: " + str(radius) + "km")

    if parentName != "UNDEFINED":
        print("Ouder: " + str(parentName) + " (" + str(parentID) + ")")
    else:
        print("Ouder: OORSPRONG")


# Exporteer de verkregen data
with open(DEST, "w") as outfile:
    json.dump(data, outfile, sort_keys = True, indent = 4, ensure_ascii = True)

print("Data is succesvol geexporteerd als " + DEST)
