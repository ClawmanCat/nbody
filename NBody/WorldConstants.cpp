// Copyright (c) Rob Klein Ikink, 2018. Licensed under CC-BY 4.0
// http://creativecommons.org/licenses/by/4.0/deed.en_US

#include "WorldConstants.h"
#include "Engine.h"

namespace nbody {
    const double WorldConstants::GRAVITATIONAL_CONSTANT = Engine::CONFIG.getValue<double>("gravitatie_constante", 6.674e-11);
}