// Copyright (c) Rob Klein Ikink, 2018. Licensed under CC-BY 4.0
// http://creativecommons.org/licenses/by/4.0/deed.en_US

#include "World.h"
#include "WorldConstants.h"
#include "WorldPhysics.h"
#include "MathUtils.h"
#include "Engine.h"

#include <cmath>

#include <iostream>

namespace nbody {
    World::World(
        boost::uuids::uuid id,
        std::string name,
        double mass,
        double radius,
        ColorRGB mapTraceColor,
        Position3<double> position,
        Position3<double> velocity,
        bool isMovable,
        bool hasGravity
    ) : id(id), name(name), bodyMass(mass), bodyRadius(radius), mapTraceColor(mapTraceColor), position(position),
        velocity(velocity), isMovable(isMovable), hasGravity(hasGravity) {
    }

    // Simulate this world for a specified amount of time in seconds. (dt)
    void World::tick_gravity(std::unordered_set<World*> others, float dt) {
        for (World* world : others) {
            if (world != this) WorldPhysics::applyGravity(this, world, dt);
        }
    }

    void World::tick_movement(std::unordered_set<World*> others, float dt) {
        this->position += this->velocity * dt;
    }

    void World::tick_collision(std::unordered_set<World*> others, float dt) {
        for (World* world : others) {
            if (world != this && MathUtils::getDistance(this->position, world->position) < this->bodyRadius + world->bodyRadius) {
                World* newWorld = WorldPhysics::collideWorlds(this, world);
                
                Engine::universe.removeWorlds({this, world});
                Engine::renderer.removeObject(this);
                Engine::renderer.removeObject(world);

                Engine::universe.addWorlds({ newWorld });
                Engine::renderer.addObject(newWorld);

                std::cout << "Botsing tussen " << this->getName() << " en " << world->getName() << std::endl;
                break;
            }
        }
    }

    // Special implementation to prevent immovable worlds from moving.
    void World::setVelocity(const Position3<double>& vel) {
        if (this->isMovable) this->velocity = vel;
    }
}