// Copyright (c) Rob Klein Ikink, 2018. Licensed under CC-BY 4.0
// http://creativecommons.org/licenses/by/4.0/deed.en_US

#include "InputManager.h"
#include "Engine.h"
#include "InputEvent.h"

namespace nbody {
    void InputManager::update(void) {
        SDL_Event evnt;

        mwheelPrevTick = mwheelCurrent;

        while (SDL_PollEvent(&evnt)) {
            switch (evnt.type) {
            case SDL_KEYDOWN:
            {
                auto it = keyMap.find(evnt.key.keysym.sym);
                if (it == keyMap.end()) {
                    keyMap.insert({ evnt.key.keysym.sym, true });
                } else {
                    it->second = true;
                }

                Engine::EVENT_DISPATCHER.dispatchEvent(new KeyEvent(evnt.key.keysym.sym, true));

                break;
            }
            case SDL_KEYUP:
            {
                auto it = keyMap.find(evnt.key.keysym.sym);
                if (it == keyMap.end()) {
                    keyMap.insert({ evnt.key.keysym.sym, false });
                } else {
                    it->second = false;
                }

                Engine::EVENT_DISPATCHER.dispatchEvent(new KeyEvent(evnt.key.keysym.sym, false));

                break;
            }
            case SDL_MOUSEBUTTONDOWN:
            {
                MouseButton button;

                switch (evnt.button.button) {
                case SDL_BUTTON_LEFT:
                    button = MouseButton::LEFT;
                    break;
                case SDL_BUTTON_RIGHT:
                    button = MouseButton::RIGHT;
                    break;
                case SDL_BUTTON_MIDDLE:
                    button = MouseButton::MIDDLE;
                    break;
                case SDL_BUTTON_X1:
                    button = MouseButton::X1;
                    break;
                case SDL_BUTTON_X2:
                    button = MouseButton::X2;
                    break;
                default:
                    //Engine::LOGGER.info("Unknown mouse button: " + toStr(evnt.button.button));
                    break;
                }

                MouseEvent mouseEvent;
                mouseEvent.mouseBegin = { evnt.button.x, evnt.button.y };
                mouseEvent.mouseCurrent = { evnt.button.x, evnt.button.y };
                mouseEvent.mwheelBegin = 0;
                mouseEvent.mwheelCurrent = 0;
                mouseEvent.button = button;
                mouseEvent.hasEnded = false;

                auto it = mouseMap.find(button);
                if (it == mouseMap.end()) {
                    mouseMap.insert({ button, mouseEvent });
                } else {
                    it->second = mouseEvent;
                }

                Engine::EVENT_DISPATCHER.dispatchEvent(new MouseButtonEvent(mouseEvent));

                break;
            }
            case SDL_MOUSEBUTTONUP:
            {
                MouseButton button;

                switch (evnt.button.button) {
                case SDL_BUTTON_LEFT:
                    button = MouseButton::LEFT;
                    break;
                case SDL_BUTTON_RIGHT:
                    button = MouseButton::RIGHT;
                    break;
                case SDL_BUTTON_MIDDLE:
                    button = MouseButton::MIDDLE;
                    break;
                case SDL_BUTTON_X1:
                    button = MouseButton::X1;
                    break;
                case SDL_BUTTON_X2:
                    button = MouseButton::X2;
                    break;
                default:
                    //Engine::LOGGER.warning("Unknown mouse button: " + toStr(evnt.button.button));
                    break;
                }

                auto it = mouseMap.find(button);
                if (it != mouseMap.end()) it->second.hasEnded = true;

                break;
            }
            case SDL_MOUSEMOTION:
            {
                int32_t nx = evnt.motion.x;
                int32_t ny = evnt.motion.y;

                for (auto& mouseEvent : mouseMap) {
                    if (!mouseEvent.second.hasEnded) {
                        mouseEvent.second.mouseCurrent.first += nx;
                        mouseEvent.second.mouseCurrent.second += ny;
                    }
                }

                break;
            }
            case SDL_MOUSEWHEEL:
            {
                int32_t motion = evnt.wheel.y;
                if (evnt.wheel.direction == SDL_MOUSEWHEEL_FLIPPED) motion *= -1;

                for (auto& mouseEvent : mouseMap) {
                    if (!mouseEvent.second.hasEnded) {
                        mouseEvent.second.mwheelCurrent += motion;
                    }
                }

                mwheelPrevTick = mwheelCurrent;
                mwheelCurrent += motion;

                break;
            }
            case SDL_QUIT:
            {
                auto it = windowMap.find(WindowEvent::EXIT);
                if (it == windowMap.end()) {
                    windowMap.insert({ WindowEvent::EXIT, true });
                } else {
                    it->second = true;
                }

                Engine::EVENT_DISPATCHER.dispatchEvent(new WindowChangedEvent(WindowEvent::EXIT, true));

                break;
            }
            case SDL_WINDOWEVENT:
            {
                switch (evnt.window.event) {
                case SDL_WINDOWEVENT_MOVED:
                {
                    auto it = windowMap.find(WindowEvent::MOVED);
                    if (it == windowMap.end()) {
                        windowMap.insert({ WindowEvent::MOVED, true });
                    } else {
                        it->second = true;
                    }

                    Engine::EVENT_DISPATCHER.dispatchEvent(new WindowChangedEvent(WindowEvent::MOVED, true));

                    break;
                }
                case SDL_WINDOWEVENT_MINIMIZED:
                case SDL_WINDOWEVENT_MAXIMIZED:
                case SDL_WINDOWEVENT_RESTORED:
                case SDL_WINDOWEVENT_RESIZED:
                {
                    auto it = windowMap.find(WindowEvent::RESIZED);
                    if (it == windowMap.end()) {
                        windowMap.insert({ WindowEvent::RESIZED, true });
                    } else {
                        it->second = true;
                    }

                    Engine::EVENT_DISPATCHER.dispatchEvent(new WindowChangedEvent(WindowEvent::RESIZED, true));

                    break;
                }
                case SDL_WINDOWEVENT_TAKE_FOCUS:
                case SDL_WINDOWEVENT_FOCUS_GAINED:
                {
                    auto it = windowMap.find(WindowEvent::FOCUSSED);
                    if (it == windowMap.end()) {
                        windowMap.insert({ WindowEvent::FOCUSSED, true });
                    } else {
                        it->second = true;
                    }

                    Engine::EVENT_DISPATCHER.dispatchEvent(new WindowChangedEvent(WindowEvent::FOCUSSED, true));

                    break;
                }
                case SDL_WINDOWEVENT_FOCUS_LOST:
                {
                    auto it = windowMap.find(WindowEvent::FOCUSSED);
                    if (it == windowMap.end()) {
                        windowMap.insert({ WindowEvent::FOCUSSED, false });
                    } else {
                        it->second = false;
                    }

                    Engine::EVENT_DISPATCHER.dispatchEvent(new WindowChangedEvent(WindowEvent::FOCUSSED, false));

                    break;
                }
                default:
                    //Engine::LOGGER.debug("Uncaptured event: " + toStr(evnt.window.type));
                    break;
                }

                break;
            }
            default:
                //Engine::LOGGER.debug("Uncaptured event: " + toStr(evnt.type));
                break;
            }
        }
    }

    void InputManager::registerKeybinding(std::string name, KeyBinding key) {
        keybindings.insert({ name, key });
    }

    bool InputManager::isKeyPressed(SDL_Keycode key) {
        auto it = keyMap.find(key);
        return (it == keyMap.end()) ? false : it->second;
    }

    bool InputManager::isMousePressed(MouseButton button) {
        auto it = mouseMap.find(button);
        return (it == mouseMap.end()) ? false : !it->second.hasEnded;
    }

    bool InputManager::hasWindowChanged(WindowEvent evnt) {
        auto it = windowMap.find(evnt);
        return (it == windowMap.end()) ? (evnt == WindowEvent::FOCUSSED) : it->second;
    }

    void InputManager::markKnown(WindowEvent evnt) {
        if (evnt == WindowEvent::FOCUSSED) return;

        auto it = windowMap.find(evnt);
        if (it == windowMap.end()) {
            windowMap.insert({ evnt, false });
        } else {
            it->second = false;
        }

        Engine::EVENT_DISPATCHER.dispatchEvent(new WindowChangedEvent(evnt, false));
    }

    bool InputManager::isKeyBindingPressed(std::string bind) {
        auto it = keybindings.find(bind);
        if (it == keybindings.end()) return false;

        switch (it->second.type) {
        case KeyBinding::KEYBOARD:
        {
            auto nit = keyMap.find(it->second.value.key);
            return (nit == keyMap.end()) ? false : nit->second;
        }
        case KeyBinding::MOUSE:
        {
            auto nit = mouseMap.find(it->second.value.mouse);
            return (nit == mouseMap.end()) ? false : !nit->second.hasEnded;
        }
        default:
            //Engine::LOGGER.warning("Unknown keybinding type: " + toStr(it->second.type) + " for keybinding " + bind);
            return false;
        }
    }
}