// Copyright (c) Rob Klein Ikink, 2018. Licensed under CC-BY 4.0
// http://creativecommons.org/licenses/by/4.0/deed.en_US

#pragma once

#include "Float128.h"
#include "Position3.h"

#include <boost/multiprecision/cpp_int.hpp>
#include <glm/glm.hpp>
#include <glm/mat3x3.hpp>

#include <cmath>

#define RAD(x) (x * M_PI / 180.0)
#define DEG(x) (x * 180.0 / M_PI)

namespace nbody {
    class MathUtils {
    public:
        static Position3<glm::highp_mat3x3> makeRotationMatrices(const Position3<double>& rotation) {
            return Position3<glm::highp_mat3x3>({
                {
                    1,                  0,                  0,
                    0,                  cos(rotation.x),    -sin(rotation.x),
                    0,                  sin(rotation.x),    cos(rotation.x)
                },

                {
                    cos(rotation.y),    0,                  sin(rotation.y),
                    0,                  1,                  0,
                    -sin(rotation.y),   0,                  cos(rotation.y)
                },

                {
                    cos(rotation.z),    -sin(rotation.z),   0,
                    sin(rotation.z),    cos(rotation.z),    0,
                    0,                  0,                  1
                }
                });
        }

        template <class T, class R> static Position3<T> translateFrameOfReference(const Position3<T>& parent, const Position3<T>& child, const Position3<R>& rotation) {
            Position3<glm::highp_mat3x3> nrotation = makeRotationMatrices(rotation);
            glm::highp_mat3x3 inverseRotation = glm::inverse(nrotation.x * nrotation.y * nrotation.z);

            Position3<T> result = child.toGLMVector3() * inverseRotation;
            result += parent;

            return result;
        }

        template <class T> static T magnitude(const Position3<T>& pos) {
            return MathUtils::mp_sqrt<T>(pos.dot(pos));
        }

        template <class T> static Position3<T> normalize(const Position3<T>& pos) {
            return Position3<T>(glm::normalize(pos.toGLMVector3()));
        }

        template <class T> static T getDistance(const Position3<T>& a, const Position3<T>& b) {
            return MathUtils::mp_sqrt<T>(getDistanceSquared(a, b));
        }

        template <class T> static T getDistanceSquared(const Position3<T>& a, const Position3<T>& b) {
            return ((b.x - a.x) * (b.x - a.x)) + ((b.y - a.y) * (b.y - a.y)) + ((b.z - a.z) * (b.z - a.z));
        }

        template <class T> static T mod(const T& a, const T& b) {
            return a - (((int)a / (int)b)*b);
        }

        template <class T> static T mp_sqrt(const T& val) {
            return (T)sqrt((const double)val);
        }

        template <> static float128 mp_sqrt(const float128& val) { return boost::multiprecision::sqrt(val); }
        template <> static boost::multiprecision::int128_t  mp_sqrt(const boost::multiprecision::int128_t&  val) { return boost::multiprecision::sqrt(val); }
        template <> static boost::multiprecision::uint128_t mp_sqrt(const boost::multiprecision::uint128_t& val) { return boost::multiprecision::sqrt(val); }
        template <> static boost::multiprecision::int256_t  mp_sqrt(const boost::multiprecision::int256_t&  val) { return boost::multiprecision::sqrt(val); }
        template <> static boost::multiprecision::uint256_t mp_sqrt(const boost::multiprecision::uint256_t& val) { return boost::multiprecision::sqrt(val); }
    private:
        MathUtils(void);
    };
}