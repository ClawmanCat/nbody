// Copyright (c) Rob Klein Ikink, 2018. Licensed under CC-BY 4.0
// http://creativecommons.org/licenses/by/4.0/deed.en_US

#pragma once

#include "Universe.h"
#include "EventDispatcher.h"
#include "WindowManager.h"
#include "InputManager.h"
#include "UniverseRenderer.h"
#include "Camera.h"
#include "Configuration.h"

#include <string>

namespace nbody {
    class Engine {
    public:
        static Universe universe;
        static UniverseRenderer renderer;

        static EventDispatcher EVENT_DISPATCHER;
        static WindowManager WINDOW;
        static InputManager INPUT;
        static Camera CAMERA;
        static Configuration CONFIG;

        static void run(int32_t argc, char** argv);
        static void exit(void);

        const static std::string PATH_CONFIGURATION;

        static double dt;
        static double tpf;
    private:
        Engine(void);
    };
}