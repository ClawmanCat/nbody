// Copyright (c) Rob Klein Ikink, 2018. Licensed under CC-BY 4.0
// http://creativecommons.org/licenses/by/4.0/deed.en_US

#pragma once

#pragma warning(push)
#pragma warning(disable:4267)

#include "Exception.h"
#include "StringUtils.h"
#include "Float128.h"

#include <boost/multiprecision/cpp_int.hpp>

#include <vector>
#include <cstdint>
#include <initializer_list>
#include <type_traits>

namespace nbody {
    class VectorUtils {
    public:
        // Vector Utility Functions

        // Extract part of a vector from begin (inclusive) to end (exclusive)
        template <class T> static std::vector<T> extractVector(const std::vector<T>& vec, uint32_t begin, uint32_t end) {
            if (begin >= end || end > vec.size()) throw new IllegalArgumentException(StringUtils::makeRange(begin, end) + " is not a valid range for a vector of size " + toStr(vec.size()));

            std::vector<T> nvec = std::vector<T>();
            nvec.reserve(end - begin);
            nvec.insert(nvec.end(), vec.begin() + begin, vec.begin() + end);

            return nvec;
        }

        // Append a set of vectors to another vector
        template <class T> static void mergeVectors(std::vector<T>& dest, std::initializer_list<std::vector<T>> vecs) {
            uint32_t size = 0;
            for (auto& vec : vecs) {
                size += vec.size();
            }

            dest.reserve(dest.size() + size);

            for (auto& vec : vecs) {
                dest.insert(dest.end(), vec.begin(), vec.end());
            }
        }

        // Easy serialization and deserialization of objects.
        template <class T> static std::vector<uint8_t> toByteArray(const T& obj) {
            if (!std::is_trivially_copyable<T>::value) throw new IllegalArgumentException("Cannot serialize non trivially copyable object.");

            const uint8_t* ptr = reinterpret_cast<const uint8_t*>(&obj);
            std::vector<uint8_t> vec = std::vector<uint8_t>(ptr, ptr + sizeof(T));

            return vec;
        }

        template <class T> static T fromByteArray(const std::vector<uint8_t>& vec) {
            if (!std::is_trivially_copyable<T>::value) throw new IllegalArgumentException("Cannot deserialize non trivially copyable object.");

            if (vec.size() != sizeof(T)) throw new IllegalArgumentException("Cannot deserialize object from byte array of invalid length. Expected: " + toStr(sizeof(T)) + ", Found: " + toStr(vec.size()) + ".");

            std::vector<uint8_t> cpy = vec;
            T* ptr = reinterpret_cast<T*>(&cpy[0]);
            return *ptr;
        }

        // String specialization for serialization and deserialization.
        template <> static std::vector<uint8_t> toByteArray<std::string>(const std::string& obj) {
            const uint8_t* ptr = reinterpret_cast<const uint8_t*>(obj.c_str());
            std::vector<uint8_t> vec = std::vector<uint8_t>(ptr, ptr + obj.length() + 1); // Account for null-terminator

            return vec;
        }

        template <> static std::string fromByteArray<std::string>(const std::vector<uint8_t>& vec) {
            if ((const char)vec.back() != '\0') throw new IllegalArgumentException("Cannot deserialize string from byte array not ending in a null terminator.");

            std::vector<uint8_t> cpy = vec;
            const char* ptr = reinterpret_cast<const char*>(&cpy[0]);
            return std::string(ptr, ptr + vec.size());
        }

        // Boost Multiprecision specialization for serialization and deserialization.
        template <> static std::vector<uint8_t> toByteArray<boost::multiprecision::int128_t>(const boost::multiprecision::int128_t& obj) { return toByteArrayMultiprecisionInternal  <boost::multiprecision::int128_t>(obj); }
        template <> static boost::multiprecision::int128_t fromByteArray<boost::multiprecision::int128_t>(const std::vector<uint8_t>& vec) { return fromByteArrayMultiprecisionInternal<boost::multiprecision::int128_t>(vec); }

        template <> static std::vector<uint8_t> toByteArray<boost::multiprecision::uint128_t>(const boost::multiprecision::uint128_t& obj) { return toByteArrayMultiprecisionInternal  <boost::multiprecision::uint128_t>(obj); }
        template <> static boost::multiprecision::uint128_t fromByteArray<boost::multiprecision::uint128_t>(const std::vector<uint8_t>& vec) { return fromByteArrayMultiprecisionInternal<boost::multiprecision::uint128_t>(vec); }

        template <> static std::vector<uint8_t> toByteArray<boost::multiprecision::int256_t>(const boost::multiprecision::int256_t& obj) { return toByteArrayMultiprecisionInternal  <boost::multiprecision::int256_t>(obj); }
        template <> static boost::multiprecision::int256_t fromByteArray<boost::multiprecision::int256_t>(const std::vector<uint8_t>& vec) { return fromByteArrayMultiprecisionInternal<boost::multiprecision::int256_t>(vec); }

        template <> static std::vector<uint8_t> toByteArray<boost::multiprecision::uint256_t>(const boost::multiprecision::uint256_t& obj) { return toByteArrayMultiprecisionInternal  <boost::multiprecision::uint256_t>(obj); }
        template <> static boost::multiprecision::uint256_t fromByteArray<boost::multiprecision::uint256_t>(const std::vector<uint8_t>& vec) { return fromByteArrayMultiprecisionInternal<boost::multiprecision::uint256_t>(vec); }

        template <> static std::vector<uint8_t> toByteArray<boost::multiprecision::int512_t>(const boost::multiprecision::int512_t& obj) { return toByteArrayMultiprecisionInternal  <boost::multiprecision::int512_t>(obj); }
        template <> static boost::multiprecision::int512_t fromByteArray<boost::multiprecision::int512_t>(const std::vector<uint8_t>& vec) { return fromByteArrayMultiprecisionInternal<boost::multiprecision::int512_t>(vec); }

        template <> static std::vector<uint8_t> toByteArray<boost::multiprecision::uint512_t>(const boost::multiprecision::uint512_t& obj) { return toByteArrayMultiprecisionInternal  <boost::multiprecision::uint512_t>(obj); }
        template <> static boost::multiprecision::uint512_t fromByteArray<boost::multiprecision::uint512_t>(const std::vector<uint8_t>& vec) { return fromByteArrayMultiprecisionInternal<boost::multiprecision::uint512_t>(vec); }

        template <> static std::vector<uint8_t> toByteArray<boost::multiprecision::int1024_t>(const boost::multiprecision::int1024_t& obj) { return toByteArrayMultiprecisionInternal  <boost::multiprecision::int1024_t>(obj); }
        template <> static boost::multiprecision::int1024_t fromByteArray<boost::multiprecision::int1024_t>(const std::vector<uint8_t>& vec) { return fromByteArrayMultiprecisionInternal<boost::multiprecision::int1024_t>(vec); }

        template <> static std::vector<uint8_t> toByteArray<boost::multiprecision::uint1024_t>(const boost::multiprecision::uint1024_t& obj) { return toByteArrayMultiprecisionInternal  <boost::multiprecision::uint1024_t>(obj); }
        template <> static boost::multiprecision::uint1024_t fromByteArray<boost::multiprecision::uint1024_t>(const std::vector<uint8_t>& vec) { return fromByteArrayMultiprecisionInternal<boost::multiprecision::uint1024_t>(vec); }


        template <> static std::vector<uint8_t> toByteArray<float128>(const float128& obj) {
            std::vector<uint8_t> vec = std::vector<uint8_t>();

            vec.push_back(obj > 0 ? 1 : 0); // Store the sign
            boost::multiprecision::export_bits(boost::multiprecision::cpp_int(obj.backend().bits()), std::back_inserter(vec), sizeof(uint8_t) * 8);

            std::vector<uint8_t> exp = VectorUtils::toByteArray<int32_t>(obj.backend().exponent());
            VectorUtils::mergeVectors(vec, { exp });

            return vec;
        }

        template <> static float128 fromByteArray<float128>(const std::vector<uint8_t>& vec) {
            boost::multiprecision::cpp_int i;
            boost::multiprecision::import_bits(i, vec.begin() + 1, vec.end() - sizeof(uint32_t));

            float128 f(i);
            f.backend().exponent() = VectorUtils::fromByteArray<int32_t>(VectorUtils::extractVector(vec, vec.size() - sizeof(uint32_t), vec.size()));
            if (vec[0] == 0) f *= -1; // Extract the sign

            return f;
        }
    private:
        // For boost multiprecision integer types.
        template <class T> static std::vector<uint8_t> toByteArrayMultiprecisionInternal(const T& obj) {
            std::vector<uint8_t> vec = std::vector<uint8_t>();
            vec.push_back(obj > 0 ? 1 : 0); // Store the sign
            boost::multiprecision::export_bits(obj, std::back_inserter(vec), sizeof(uint8_t) * 8);

            return vec;
        }

        template <class T> static T fromByteArrayMultiprecisionInternal(const std::vector<uint8_t>& vec) {
            T val;

            boost::multiprecision::import_bits(val, vec.begin() + 1, vec.end());
            if (vec[0] == 0) val *= -1; // Extract the sign

            return val;
        }

        VectorUtils(void);
    };
}

#pragma warning(pop)