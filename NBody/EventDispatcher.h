// Copyright (c) Rob Klein Ikink, 2018. Licensed under CC-BY 4.0
// http://creativecommons.org/licenses/by/4.0/deed.en_US

#pragma once

#include "Event.h"
#include "Utility.h"

#include <functional>
#include <vector>
#include <utility>
#include <type_traits>

namespace nbody {
    class EventDispatcher {
    public:
        EventDispatcher(void) : listeners(std::vector<std::pair<EventTypeBase*, std::vector<std::function<void(Event*)>>>>()) {}

        ~EventDispatcher(void) {
            for (auto& pair : listeners) delete pair.first;
        }

        template <class EventType> void addListener(std::function<void(Event*)> listener) {
            static_assert(std::is_base_of<Event, EventType>::value, "Given type parameter does not implement Event.");

            // Add the listener to an existing list, if one exists.
            for (auto& pair : listeners) {
                if (Utility::instanceof<EventTypeContainer<EventType>>(pair.first)) {
                    pair.second.push_back(listener);
                    return;
                }
            }

            // No existing list, so make a new one.
            EventTypeBase* typeContainer = new EventTypeContainer<EventType>();
            std::vector<std::function<void(Event*)>> listenerVec = { listener };

            listeners.push_back(std::make_pair(typeContainer, listenerVec));
        }

        void dispatchEvent(Event* evnt) {
            for (auto& pair : listeners) {
                if (pair.first->isEventType(evnt)) {
                    for (auto& func : pair.second) func(evnt);
                }
            }

            delete evnt;
        }
    private:
        struct EventTypeBase abstract {
            virtual bool isEventType(Event* evnt) = 0;
        };

        template <class T> struct EventTypeContainer : public EventTypeBase {
            virtual bool isEventType(Event* evnt) {
                return Utility::instanceof<T>(evnt);
            }
        };

        std::vector<std::pair<EventTypeBase*, std::vector<std::function<void(Event*)>>>> listeners;
    };
}