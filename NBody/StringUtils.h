// Copyright (c) Rob Klein Ikink, 2018. Licensed under CC-BY 4.0
// http://creativecommons.org/licenses/by/4.0/deed.en_US

#pragma once

// Warning about the toStr() macro, that I've not managed to figure out.
// The macro itself seems to be working fine however...
#pragma warning(push)
#pragma warning(disable:4067)
#pragma warning(disable:4267)

#include <string>
#include <sstream>
#include <errno.h>
#include <iomanip>

// Macro for easier access to toString()
#ifndef toStr(a)
#define toStr(a) nbody::StringUtils::toString(a)
#endif

namespace nbody {
    class StringUtils {
    public:
        template <class A, class B> static std::string makeRange(const A& begin, const B& end) {
            std::stringstream stream;
            stream << "[" << begin << ", " << end << "]";

            return stream.str();
        }

        template <class X, class Y, class Z> static std::string makePosition(const X& x, const Y& y, const Z& z) {
            std::stringstream stream;
            stream << "[" << x << ", " << y << ", " << z << "]";

            return stream.str();
        }

        static std::string getError(void) {
            return std::string(strerror(errno));
        }

        template <class T> static std::string toString(const T& obj) {
            std::stringstream stream;
            stream << obj;

            return stream.str();
        }

        template <> static std::string toString<bool>(const bool& obj) {
            return (obj) ? "true" : "false";
        }

        static std::string leftPad(std::string str, uint32_t amount, char ch = ' ') {
            std::stringstream stream = std::stringstream();
            stream << std::setfill(ch) << std::setw(amount + str.length()) << str;

            return stream.str();
        }

        static std::string rightPad(std::string str, uint32_t amount, char ch = ' ') {
            std::stringstream stream = std::stringstream();
            stream << str << std::setfill(ch) << std::setw(amount) << "";

            return stream.str();
        }

        static std::string leftPadTo(std::string str, uint32_t length, char ch = ' ') {
            return str.length() > length ? str.substr(0, length) : leftPad(str, length - str.length(), ch);
        }

        static std::string rightPadTo(std::string str, uint32_t length, char ch = ' ') {
            return str.length() > length ? str.substr(0, length) : rightPad(str, length - str.length(), ch);
        }
    private:
        StringUtils(void);
    };
}

#pragma warning(pop)