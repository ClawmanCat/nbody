// Copyright (c) Rob Klein Ikink, 2018. Licensed under CC-BY 4.0
// http://creativecommons.org/licenses/by/4.0/deed.en_US

#include "Camera.h"
#include "Engine.h"
#include "InputEvent.h"
#include "InputManager.h"

#include <SDL/SDL_keyboard.h>

#include <iostream>

namespace nbody {
    const double Camera::CAMERA_MOVE_SPEED = 5;

    Camera::Camera(void) : pos({ 0.0, 0.0, 0.0 }), scale(1e-7), cameraMatrix(1.0) {
        Engine::EVENT_DISPATCHER.addListener<KeyEvent>([this](Event* evnt) { this->translateOnKeyInput(evnt); });

        updateMatrix();
    }

    void Camera::move(Position3<double> pos) {
        this->pos += pos;
        updateMatrix();
    }

    void Camera::zoom(double scl) {
        this->scale *= scl;
        updateMatrix();
    }

    void Camera::setPosition(Position3<double> pos) {
        this->pos = pos;
        updateMatrix();
    }

    void Camera::setScale(double scale) {
        this->scale = scale;
        updateMatrix();
    }

    void Camera::reset(void) {
        this->pos = { 0.0, 0.0, 0.0 };
        this->scale = 1e-8;

        updateMatrix();
    }

    void Camera::applyCamera(Position3<double>& vertex) {
        glm::vec4 vpos = cameraMatrix * glm::vec4(vertex.x, vertex.y, 0.0, 1.0);
        vertex.x = vpos.x;
        vertex.y = vpos.y;
    }

    void Camera::updateMatrix(void) {
        auto size = Engine::WINDOW.getWindowSize();

        glm::mat4 orthoMatrix = glm::ortho((double) -size.first / 2.0, (double) size.first / 2.0, (double) -size.second / 2.0, (double) size.second / 2.0);
        this->cameraMatrix = glm::scale(orthoMatrix, glm::vec3(scale, scale, 1.0));
        this->cameraMatrix = glm::translate(cameraMatrix, glm::vec3(-pos.x, -pos.y, 0.0));
    }

    void Camera::translateOnKeyInput(Event* evnt) {
        KeyEvent* kEvent = (KeyEvent*) evnt;

        if (kEvent->key == SDLK_d) move({  CAMERA_MOVE_SPEED * (1.0 / scale), 0.0, 0.0 });
        if (kEvent->key == SDLK_a) move({ -CAMERA_MOVE_SPEED * (1.0 / scale), 0.0, 0.0 });
        if (kEvent->key == SDLK_w) move({  0.0,  CAMERA_MOVE_SPEED * (1.0 / scale), 0.0 });
        if (kEvent->key == SDLK_s) move({  0.0, -CAMERA_MOVE_SPEED * (1.0 / scale), 0.0 });

        if (kEvent->key == SDLK_SPACE) reset();
    }
}