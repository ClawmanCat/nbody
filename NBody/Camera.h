// Copyright (c) Rob Klein Ikink, 2018. Licensed under CC-BY 4.0
// http://creativecommons.org/licenses/by/4.0/deed.en_US

#pragma once

#include "Position3.h"
#include "Event.h"

#include <GLM/glm.hpp>
#include <GLM/gtc/matrix_transform.hpp>

namespace nbody {
    class Camera {
    public:
        const static double CAMERA_MOVE_SPEED;

        Camera(void);

        void move(Position3<double> pos);
        void zoom(double scl);

        void setPosition(Position3<double> pos);
        void setScale(double scale);

        void reset(void);

        void applyCamera(Position3<double>& vertex);

		Position3<double> getPosition(void) const { return pos; }
    private:
        Position3<double> pos;
        double scale;

        glm::mat4 cameraMatrix;


        void updateMatrix(void);
        void translateOnKeyInput(Event* evnt);
    };
}