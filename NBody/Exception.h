// Copyright (c) Rob Klein Ikink, 2018. Licensed under CC-BY 4.0
// http://creativecommons.org/licenses/by/4.0/deed.en_US

#pragma once

#include <string>

namespace nbody {
    class Exception abstract {
    public:
        Exception(std::string message)  : message(message)          {}
        Exception(void)                 : Exception("No Message.")  {}

        std::string getMessage(void) const { return this->message; }
    private:
        const std::string message;
    };

    class IllegalArgumentException                              : public Exception { using Exception::Exception; };
    class IOException                                           : public Exception { using Exception::Exception; };
    class SDLException                                          : public Exception { using Exception::Exception; };
    class GLEWException                                         : public Exception { using Exception::Exception; };
    class FreeTypeException                                     : public Exception { using Exception::Exception; };
}