// Copyright (c) Rob Klein Ikink, 2018. Licensed under CC-BY 4.0
// http://creativecommons.org/licenses/by/4.0/deed.en_US

#pragma once

#include "World.h"
#include "Position3.h"

#include <queue>
#include <unordered_map>
#include <cstdint>

namespace nbody {
    class UniverseRenderer {
    public:
        const static uint32_t HISTORY_LIMIT = 10000;

        const static int64_t DIMENSIONS = 2e11;

        UniverseRenderer(void);

        void addObject(World* world);
        void removeObject(World* world);

        void render(void);
    private:
        template <class T> T _scale(T oldVal, T oldMin, T oldMax, T newMin, T newMax) {
            return (newMax - newMin) * (oldVal - oldMin) / (oldMax - oldMin) + newMin;
        }

        static long frame;
        std::unordered_map<World*, std::deque<Position3<double>>> history;
    };
}