// Copyright (c) Rob Klein Ikink, 2018. Licensed under CC-BY 4.0
// http://creativecommons.org/licenses/by/4.0/deed.en_US

#include "Engine.h"

#include <cstdint>

int32_t main(int32_t argc, char** argv) {
    // Program Entry Point
    using namespace nbody;

    Engine::run(argc, argv);

    return 1;
}