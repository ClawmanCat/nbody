// Copyright (c) Rob Klein Ikink, 2018. Licensed under CC-BY 4.0
// http://creativecommons.org/licenses/by/4.0/deed.en_US

#pragma once

#include "Position3.h"
#include "WorldConstants.h"
#include "World.h"
#include "MathUtils.h"
#include "Float128.h"

#include <boost/uuid/uuid_generators.hpp>

#include <utility>
#include <cmath>

#include <iostream>

namespace nbody {
    class WorldPhysics {
    public:
        static bool doesBodyOcclude(World* source, World* parent, World* child) {
            auto a = parent->getPosition();
            auto z = source->getPosition();
            auto m = child->getPosition();

            //double t = ((z - a).dot(m - z)) / (pow(MathUtils::magnitude(m - z), 2));
            //double d = sqrt(pow((z.x - a.x) + (m.x - z.x) * t, 2) + pow((z.y - a.y) + (m.y - z.y) * t, 2) + pow((z.z - a.z) + (m.z - z.z) * t, 2));

            double d = (MathUtils::magnitude((a - z).cross(a - m)) / MathUtils::magnitude(m - z));

            //std::cout << d << std::endl;
            return d < parent->getBodyRadius();
        }

        static World* collideWorlds(World* a, World* b) {
            Position3<double> newVelocity = ((a->getMass() * a->getVelocity()) + (b->getMass() * b->getVelocity())) / (a->getMass() + b->getMass());
            
            double va = (4.0 / 3.0) * M_PI * a->getBodyRadius() * a->getBodyRadius() * a->getBodyRadius();
            double vb = (4.0 / 3.0) * M_PI * b->getBodyRadius() * b->getBodyRadius() * b->getBodyRadius();
            double newRadius = std::cbrt((va + vb) / (M_PI) * 0.75);

            ColorRGB ca = a->getMapTraceColor();
            ColorRGB cb = b->getMapTraceColor();

            double totalMass = a->getMass() + b->getMass();
            double aPercent = a->getMass() / totalMass;
            double bPercent = b->getMass() / totalMass;

            Position3<uint64_t> clrA = { ca.r, ca.g, ca.b };
            Position3<uint64_t> clrB = { cb.r, cb.g, cb.b };

            Position3<uint64_t> avg = ((clrA * aPercent) + (clrB * bPercent));

            ColorRGB newColor = {
                static_cast<uint8_t>(avg.x),
                static_cast<uint8_t>(avg.y),
                static_cast<uint8_t>(avg.z)
            };

            Position3<double> newPosition = ((a->getPosition() * a->getMass()) + (b->getPosition() * b->getMass())) / (a->getMass() + b->getMass());

            return new World(boost::uuids::random_generator()(), a->getName() + "-" + b->getName(), a->getMass() + b->getMass(), newRadius, newColor, newPosition, newVelocity);
        }

        static void applyGravity(World* a, World* b, float dt) {
            if (a->getPosition() < b->getPosition()) return;
            //std::cout << a->getName() << "-->" << b->getName() << std::endl;

            Position3<double> distance = a->getPosition() - b->getPosition();
            Position3<double> normalizedDistance = MathUtils::normalize(distance);
            double magnitudeDistance = MathUtils::magnitude(distance);

            double force = WorldConstants::GRAVITATIONAL_CONSTANT * ((a->getMass() * b->getMass()) / (magnitudeDistance * magnitudeDistance));

            a->setVelocity(a->getVelocity() + ((-normalizedDistance * dt * force) / a->getMass()));
            b->setVelocity(b->getVelocity() + (( normalizedDistance * dt * force) / b->getMass()));
        }

        static std::pair<Position3<double>, Position3<double>> convertKeplerOrbit(double mass, double semimajor_axis, double eccentricity, double inclination, double lt_ascending_node, double argument_periapsis, double true_anomaly) {
            // Allow shorter notation without obfuscating function definition.
            double e = eccentricity;
            double a = semimajor_axis;
            double i = RAD(inclination);
            double o = RAD(lt_ascending_node);
            double w = RAD(argument_periapsis);
            double v = RAD(true_anomaly);

            double p = a * (1 - (e * e));
            double r = p / (1 + e * cos(v));
            double h = std::sqrt(WorldConstants::GRAVITATIONAL_CONSTANT * mass * p);

            Position3<double> pos = {
                r * (cos(o) * cos(w + v) - sin(o) * sin(w + v) * cos(i)),
                r * (sin(o) * cos(w + v) + cos(o) * sin(w + v) * cos(i)),
                r * (sin(i) * sin(w + v))
            };

            Position3<double> vel = {
                ((pos.x * h * e) / (r * p)) * sin(v) - (h / r) * (cos(o) * sin(w + v) * cos(i)),
                ((pos.y * h * e) / (r * p)) * sin(v) - (h / r) * (sin(o) * sin(w + v) * cos(i)),
                ((pos.z * h * e) / (r * p)) * sin(v) + (h / r) *  sin(i) * cos(w + v)
            };

            return { pos, vel };
        }
    private:
        WorldPhysics(void);
    };
}