// Copyright (c) Rob Klein Ikink, 2018. Licensed under CC-BY 4.0
// http://creativecommons.org/licenses/by/4.0/deed.en_US

#pragma once

namespace nbody {
    class WorldConstants {
    public:
        const static double GRAVITATIONAL_CONSTANT;
    private:
        WorldConstants(void);
    };
}