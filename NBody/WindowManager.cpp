// Copyright (c) Rob Klein Ikink, 2018. Licensed under CC-BY 4.0
// http://creativecommons.org/licenses/by/4.0/deed.en_US

#include "WindowManager.h"
#include "Engine.h"
#include "InputEvent.h"

#include <algorithm>

namespace nbody {
    WindowManager::WindowManager(void) : handle(nullptr), isInitialized(false) {
        // Make sure to notify this window if someone changes it.
        Engine::EVENT_DISPATCHER.addListener<WindowChangedEvent>([this](Event* evnt) { this->onExternalWindowUpdate(evnt); });
    }

    WindowManager::~WindowManager(void) {
        if (isInitialized) destroyWindow();
    }

    void WindowManager::createWindow(std::string title, int32_t x, int32_t y, int32_t w, int32_t h, WindowMode mode) {
        this->handle = SDL_CreateWindow(title.c_str(), 0, 0, 512, 512, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
        changeWindow(title, x, y, w, h, mode); // To set the correct WindowMode and accout for special parameters like WINDOW_FIT_TO_SCREEN.

        isInitialized = true;
    }

    void WindowManager::destroyWindow(void) {
        SDL_DestroyWindow(handle);
        isInitialized = false;
    }

    void WindowManager::changeWindow(std::string title, int32_t x, int32_t y, int32_t w, int32_t h, WindowMode mode) {
        int32_t nx = (x == WINDOW_CENTERED) ? SDL_WINDOWPOS_CENTERED : x;
        int32_t ny = (y == WINDOW_CENTERED) ? SDL_WINDOWPOS_CENTERED : y;

        if (getWindowTitle() != title)                   SDL_SetWindowTitle(handle, title.c_str());
        if (getWindowPosition() != std::make_pair(nx, ny))  SDL_SetWindowPosition(handle, nx, ny);

        if (w == WINDOW_FIT_TO_SCREEN || h == WINDOW_FIT_TO_SCREEN) {
            SDL_MaximizeWindow(handle);
        } else {
            if (getWindowSize() != std::make_pair(w, h)) SDL_SetWindowSize(handle, w, h);
        }

        if (getWindowMode() != mode) {
            switch (mode) {
            case FULLSCREEN:
                //SDL_SetWindowBordered(handle, SDL_FALSE);
                SDL_SetWindowFullscreen(handle, SDL_TRUE);

                break;
            case BORDERLESS:
                SDL_SetWindowBordered(handle, SDL_FALSE);
                SDL_SetWindowFullscreen(handle, SDL_FALSE);

                break;
            case WINDOWED:
                SDL_SetWindowBordered(handle, SDL_TRUE);
                SDL_SetWindowFullscreen(handle, SDL_FALSE);

                break;
            default: _assume(0);
            }
        }
    }

    std::pair<int32_t, int32_t> WindowManager::getScreenSize(int32_t screenID) {
        SDL_DisplayMode mode;
        SDL_GetCurrentDisplayMode(screenID, &mode);
        return { mode.w, mode.h };
    }

    void WindowManager::onExternalWindowUpdate(Event* evnt) {
        WindowChangedEvent* windowEvent = (WindowChangedEvent*)evnt;
        if (windowEvent->evnt == InputManager::FOCUSSED && windowEvent->isActive) SDL_RaiseWindow(handle);
    }

    std::pair<int32_t, int32_t> WindowManager::getWindowPosition(void) {
        int32_t x, y;
        SDL_GetWindowPosition(handle, &x, &y);
        return { x, y };
    }

    std::pair<int32_t, int32_t> WindowManager::getWindowSize(void) {
        int32_t w, h;
        SDL_GetWindowSize(handle, &w, &h);
        return { w, h };
    }

    std::string WindowManager::getWindowTitle(void) {
        return std::string(SDL_GetWindowTitle(handle));
    }

    WindowManager::WindowMode WindowManager::getWindowMode(void) {
        uint32_t flags = SDL_GetWindowFlags(handle);
        bool isFullscreen = (flags & SDL_WINDOW_FULLSCREEN) || (flags & SDL_WINDOW_FULLSCREEN_DESKTOP);
        bool isBorderless = (flags & SDL_WINDOW_BORDERLESS);

        return isFullscreen ? FULLSCREEN : isBorderless ? BORDERLESS : WINDOWED;
    }

    SDL_Window*  WindowManager::getWindowHandle(void) {
        return handle;
    }
}