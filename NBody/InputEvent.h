// Copyright (c) Rob Klein Ikink, 2018. Licensed under CC-BY 4.0
// http://creativecommons.org/licenses/by/4.0/deed.en_US

#pragma once

#include "Event.h"
#include "InputManager.h"

#include <string>

namespace nbody {
    class InputEvent abstract : public Event { public: virtual ~InputEvent(void) {} };

    class KeybindingEvent : public InputEvent {
    public:
        KeybindingEvent(std::string name, const InputManager::KeyBinding& binding) : name(name), binding(binding) {}
        virtual ~KeybindingEvent(void) {}

        std::string name;
        const InputManager::KeyBinding& binding;
    };

    class KeyEvent : public InputEvent {
    public:
        KeyEvent(const SDL_Keycode& key, bool wasPressedDown) : key(key), wasPressedDown(wasPressedDown) {}
        virtual ~KeyEvent(void) {}

        const SDL_Keycode& key;
        bool wasPressedDown;
    };

    class MouseButtonEvent : public InputEvent {
    public:
        MouseButtonEvent(const InputManager::MouseEvent& evnt) : evnt(evnt) {}
        virtual ~MouseButtonEvent(void) {}

        const InputManager::MouseEvent& evnt;
    };

    class WindowChangedEvent : public InputEvent {
    public:
        WindowChangedEvent(InputManager::WindowEvent evnt, bool isActive) : evnt(evnt), isActive(isActive) {}
        virtual ~WindowChangedEvent(void) {}

        InputManager::WindowEvent evnt;
        bool isActive;
    };
}