// Copyright (c) Rob Klein Ikink, 2018. Licensed under CC-BY 4.0
// http://creativecommons.org/licenses/by/4.0/deed.en_US

#include "UniverseRenderer.h"
#include "MathUtils.h"
#include "Engine.h"

#include <GLEW/glew.h>
#include <SDL/SDL_keyboard.h>

#include <iostream>

namespace nbody {
    long UniverseRenderer::frame = 0;

    UniverseRenderer::UniverseRenderer(void) : history(std::unordered_map<World*, std::deque<Position3<double>>>()) {}

    void UniverseRenderer::addObject(World* world) {
        history.insert({ world,{ world->getPosition() } });
    }

    void UniverseRenderer::render(void) {
        bool draw_tails = Engine::CONFIG.getValue("teken_staart", true);
        uint32_t tail_length = Engine::CONFIG.getValue("staart_lengte", 1024);
        uint32_t tail_interval = Engine::CONFIG.getValue("staart_interval", 1024);

        bool updateTail = (frame % tail_interval == 0);

        for (auto& pair : history) {
            if (updateTail) pair.second.push_back(pair.first->getPosition());

            auto current = pair.first->getPosition();
            Engine::CAMERA.applyCamera(current);

            glBegin(GL_TRIANGLE_FAN);

            ColorRGB color = pair.first->getMapTraceColor();
            glColor3f((float) color.r / 255.0, (float) color.g / 255.0, (float) color.b / 255.0);

            glVertex2f(current.x, current.y);
            for (int i = 0; i <= 256; i++) {
                glVertex2f(
                    current.x + (0.01 * cos(i * (2 * M_PI) / 256)),
                    current.y + (0.01 * sin(i * (2 * M_PI) / 256))
                );
            }
            glEnd();

            
            if (draw_tails) {
                glBegin(GL_LINES);
                for (auto point : pair.second) {
                    Engine::CAMERA.applyCamera(point);
                    glVertex2f(point.x, point.y);
                }
                glEnd();
            }
        }

        for (auto& pair : history) {
            if (updateTail && pair.second.size() >= tail_length) pair.second.pop_front();
        }

        frame++;
    }

    void UniverseRenderer::removeObject(World* world) {
        this->history.erase(world);
    }
}