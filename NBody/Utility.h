// Copyright (c) Rob Klein Ikink, 2018. Licensed under CC-BY 4.0
// http://creativecommons.org/licenses/by/4.0/deed.en_US

#pragma once

#include "VectorUtils.h"

#include <type_traits>
#include <functional>
#include <unordered_set>
#include <vector>
#include <utility>

// A lot less typing than writing getter/setter functions every time.
#define MAKEGETSET(type, var, varCapital) \
type get##varCapital (void) const { return this->var; } \
void set##varCapital (const type & val) { this->var = val; }

#define MAKEGET(type, var, varCapital) \
type get##varCapital (void) const { return this->var; }

#define MAKESET(type, var, varCapital) \
void set##varCapital (const type & val) { this->var = val; }

namespace nbody {
    class Utility {
    public:
        template <class Base, class Der> static bool instanceof(const Der* derived) {
            return dynamic_cast<const Base*>(derived) != nullptr;
        }

        template <class T> static const T* findCommonAncestor(const T* a, const T* b, std::function<const T*(const T*)> getParent) {
            std::unordered_set<const T*> a_ancestors = std::unordered_set<const T*>();

            // Create a list of all of A's ancestors.
            for (const T* i = a; i != nullptr; i = getParent(i)) a_ancestors.insert(i);

            // Check if any of B's ancestors are in the list.
            for (const T* i = b; i != nullptr; i = getParent(i)) {
                auto it = a_ancestors.find(i);
                if (it != a_ancestors.end()) return *it;
            }

            // A and B have no common ancestor.
            return nullptr;
        }

        // Serialization utility function.
        template <class T> static T extractAndIncrement(const std::vector<uint8_t>& vec, uint32_t& cntr, uint32_t size = sizeof(T)) {
            T result = VectorUtils::fromByteArray<T>(VectorUtils::extractVector(vec, cntr, cntr + size));
            cntr += size;
            return result;
        }
    private:
        Utility(void);
    };
}