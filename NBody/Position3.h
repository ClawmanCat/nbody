// Copyright (c) Rob Klein Ikink, 2018. Licensed under CC-BY 4.0
// http://creativecommons.org/licenses/by/4.0/deed.en_US

#pragma once

#include "StringUtils.h"
#include "Float128.h"

#include <GLM/vec3.hpp>
#include <GLM/vec4.hpp>
#include <boost/functional/hash.hpp>
#include <boost/multiprecision/cpp_int.hpp>

#include <sstream>
#include <string>
#include <vector>
#include <cstddef>
#include <math.h>

namespace nbody {
    struct PositionWrapper abstract {};

    template <class T> struct Position3 : public PositionWrapper {
        T x, y, z;

        Position3(void) : x(0), y(0), z(0) {}
        Position3(const T& x, const T& y, const T& z) : x(x), y(y), z(z) {}
        Position3(const glm::highp_vec3& pos) : x(pos.x), y(pos.y), z(pos.z) {}
        template <class N> Position3(const Position3<N>& pos) : x(pos.x), y(pos.y), z(pos.z) {}

        // Conversions
        std::string toString(void) const { return StringUtils::makePosition(x, y, z); }

        std::vector<T>  toSTDVector(void) const { return { x, y, z }; }
        glm::highp_vec3 toGLMVector3(void) const { return { x, y, z }; }
        glm::highp_vec4 toGLMVector4(void) const { return { x, y, z, 1 }; }

        template <class N> Position3<N> cast_regular(void) { return Position3<N>((N)x, (N)y, (N)z); }
        template <class N> Position3<N> cast_boost(void) { return Position3<N>(x.convert_to<N>(), y.convert_to<N>(), z.convert_to<N>()); }

        template <class N> operator Position3<N>() const { return Position3<N>((N)x, (N)y, (N)z); }

        std::ostream& operator<<(std::ostream& stream) const {
            stream << this->toString();
            return stream;
        }

        // Comparison operators
        bool operator==(const Position3<T>& pos) const { return (this->x == pos.x && this->y == pos.y && this->z == pos.z); }
        bool operator!=(const Position3<T>& pos) const { return !(*this == pos); }

        // TODO: Fix these comparisons
        bool operator< (const Position3<T>& pos) const { return (this->x == pos.x) ? (this->y == pos.y) ? (this->z == pos.z) ? false : (this->z < pos.z) : (this->y < pos.y) : (this->x < pos.x); }
        bool operator> (const Position3<T>& pos) const { return (*this != pos) && !(*this < pos); }
        bool operator<=(const Position3<T>& pos) const { return !(*this > pos); }
        bool operator>=(const Position3<T>& pos) const { return !(*this < pos); }

        // Arithmetic operators
        template <class N> Position3<T> operator+(const Position3<N>& pos) const { return Position3<T>(this->x + pos.x, this->y + pos.y, this->z + pos.z); }
        template <class N> Position3<T> operator-(const Position3<N>& pos) const { return Position3<T>(this->x - pos.x, this->y - pos.y, this->z - pos.z); }
        template <class N> Position3<T> operator*(const Position3<N>& pos) const { return Position3<T>(this->x * pos.x, this->y * pos.y, this->z * pos.z); }
        template <class N> Position3<T> operator/(const Position3<N>& pos) const { return Position3<T>(this->x / pos.x, this->y / pos.y, this->z / pos.z); }
        template <class N> Position3<T> operator%(const Position3<N>& pos) const { return Position3<T>(this->x % pos.x, this->y % pos.y, this->z % pos.z); }

        template <class N> Position3<T>& operator+=(const Position3<N>& pos) { this->x += pos.x; this->y += pos.y; this->z += pos.z; return *this; }
        template <class N> Position3<T>& operator-=(const Position3<N>& pos) { this->x -= pos.x; this->y -= pos.y; this->z -= pos.z; return *this; }
        template <class N> Position3<T>& operator*=(const Position3<N>& pos) { this->x *= pos.x; this->y *= pos.y; this->z *= pos.z; return *this; }
        template <class N> Position3<T>& operator/=(const Position3<N>& pos) { this->x /= pos.x; this->y /= pos.y; this->z /= pos.z; return *this; }
        template <class N> Position3<T>& operator%=(const Position3<N>& pos) { this->x %= pos.x; this->y %= pos.y; this->z %= pos.z; return *this; }

        template <class N> Position3<T> operator+(const N& val) const { return Position3<T>(this->x + val, this->y + val, this->z + val); }
        template <class N> Position3<T> operator-(const N& val) const { return Position3<T>(this->x - val, this->y - val, this->z - val); }
        template <class N> Position3<T> operator*(const N& val) const { return Position3<T>(this->x * val, this->y * val, this->z * val); }
        template <class N> Position3<T> operator/(const N& val) const { return Position3<T>(this->x / val, this->y / val, this->z / val); }
        template <class N> Position3<T> operator%(const N& val) const { return Position3<T>(this->x % val, this->y % val, this->z % val); }

        template <class N> Position3<T>& operator+=(const N& val) { this->x += val; this->y += val; this->z += val; return *this; }
        template <class N> Position3<T>& operator-=(const N& val) { this->x -= val; this->y -= val; this->z -= val; return *this; }
        template <class N> Position3<T>& operator*=(const N& val) { this->x *= val; this->y *= val; this->z *= val; return *this; }
        template <class N> Position3<T>& operator/=(const N& val) { this->x /= val; this->y /= val; this->z /= val; return *this; }
        template <class N> Position3<T>& operator%=(const N& val) { this->x %= val; this->y %= val; this->z %= val; return *this; }

        Position3<T> operator-(void) const { return { -x, -y, -z }; }
        Position3<T> abs(void) const { return { (x < 0 ? -x : x), (y < 0 ? -y : y), (z < 0 ? -z : z) }; }

        template <class N> T dot(const Position3<N>& pos) const { return { this->x * pos.x + this->y * pos.y + this->z * pos.z }; }
        template <class N> Position3<T> cross(const Position3<N>& pos) const { return { this->y * pos.z - this->z * pos.y, this->z * pos.x - this->x * pos.z, this->x * pos.y - this->y * pos.x }; }

        // Use the proper hashing function instead of a stringstream.
        // (Usually around 20x faster, but doesn't work for types that don't have a defined hash function.)
        struct QuickHash {
            std::size_t operator()(const Position3<T>& pos) const {
                auto hx = std::hash<T>()(pos.x);
                auto hy = std::hash<T>()(pos.y);
                auto hz = std::hash<T>()(pos.z);

                std::size_t hash = 0;
                boost::hash_combine(hash, hx);
                boost::hash_combine(hash, hy);
                boost::hash_combine(hash, hz);

                return hash;
            }
        };

        // Fallback Hash
        struct FallbackHash {
            std::size_t operator()(const Position3<T>& pos) const {
                std::stringstream stream;
                stream << pos.x << "_" << pos.y << "_" << pos.z;

                return std::hash<std::string>()(stream.str());
            }
        };
    };

    // Arithmetic operators with the position object as RHS. (Disable for N = Position3 to prevent conflicts.)
    template <class T, class N, typename = std::enable_if<!std::is_base_of<PositionWrapper, N>::value>> Position3<T> operator+(const N& lhs, const Position3<T>& rhs) { return Position3<T>(lhs + rhs.x, lhs + rhs.y, lhs + rhs.z); }
    template <class T, class N, typename = std::enable_if<!std::is_base_of<PositionWrapper, N>::value>> Position3<T> operator-(const N& lhs, const Position3<T>& rhs) { return Position3<T>(lhs - rhs.x, lhs - rhs.y, lhs - rhs.z); }
    template <class T, class N, typename = std::enable_if<!std::is_base_of<PositionWrapper, N>::value>> Position3<T> operator*(const N& lhs, const Position3<T>& rhs) { return Position3<T>(lhs * rhs.x, lhs * rhs.y, lhs * rhs.z); }
    template <class T, class N, typename = std::enable_if<!std::is_base_of<PositionWrapper, N>::value>> Position3<T> operator/(const N& lhs, const Position3<T>& rhs) { return Position3<T>(lhs / rhs.x, lhs / rhs.y, lhs / rhs.z); }
    template <class T, class N, typename = std::enable_if<!std::is_base_of<PositionWrapper, N>::value>> Position3<T> operator%(const N& lhs, const Position3<T>& rhs) { return Position3<T>(lhs % rhs.x, lhs % rhs.y, lhs % rhs.z); }
}