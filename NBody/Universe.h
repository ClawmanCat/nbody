// Copyright (c) Rob Klein Ikink, 2018. Licensed under CC-BY 4.0
// http://creativecommons.org/licenses/by/4.0/deed.en_US

#pragma once

#include "World.h"
#include "Color.h"
#include "WorldPhysics.h"
#include "Utility.h"

#include <boost/uuid/uuid_generators.hpp>

#include <unordered_set>
#include <string>

#include <iostream>
#include "StringUtils.h"
#include "MathUtils.h"

namespace nbody {
    class Universe {
    public:
        Universe(void) {}

        void addWorld(std::string name, ColorRGB color, double mass, double radius, double semimajor_axis, double eccentricity, double inclination, double lt_ascending_node, double argument_periapsis, double true_anomaly) {
            auto pair = WorldPhysics::convertKeplerOrbit(mass, semimajor_axis, eccentricity, inclination, lt_ascending_node, argument_periapsis, true_anomaly);
            World* world = new World(boost::uuids::random_generator()(), name, mass, radius, color, pair.first, pair.second);
            worlds.insert(world);
            
            std::cout << "Wereld " << name << " is geplaatst op " << pair.first.toString() << " (" << toStr(MathUtils::magnitude(pair.first)) << ")" << " met v=" << toStr(MathUtils::magnitude(pair.second)) << std::endl;
        }

        void addWorld(World* world) {
            worlds.insert(world);

            std::cout << "Wereld " << world->getName() << " is geplaatst op " << world->getPosition().toString() << "(" << toStr(MathUtils::magnitude(world->getPosition())) << ")" << " met v=" << toStr(MathUtils::magnitude(world->getVelocity())) << std::endl;
        }

        void tick(void);

        void removeWorlds(std::unordered_set<World*> worlds) {
            rmQueue.insert(worlds.begin(), worlds.end());
        }

        void addWorlds(std::unordered_set<World*> worlds) {
            addQueue.insert(worlds.begin(), worlds.end());
        }

        MAKEGET(std::unordered_set<World*>, worlds, Worlds)
    private:
        std::unordered_set<World*> worlds;

        std::unordered_set<World*> rmQueue;
        std::unordered_set<World*> addQueue;
    };
}