// Copyright (c) Rob Klein Ikink, 2018. Licensed under CC-BY 4.0
// http://creativecommons.org/licenses/by/4.0/deed.en_US

#include "Color.h"

namespace nbody {
    ColorRGB::ColorRGB(void) : r(0), g(0), b(0) {}
    ColorRGB::ColorRGB(const uint8_t r, const uint8_t g, const uint8_t b) : r(r), g(g), b(b) {}
    ColorRGB::ColorRGB(const ColorRGB&  clr) : r(clr.r), g(clr.g), b(clr.b) {}
    ColorRGB::ColorRGB(const ColorRGBA& clr) : r(clr.r), g(clr.g), b(clr.b) {}

    ColorRGB::operator ColorRGBA() const { return ColorRGBA(r, g, b, 255); }



    ColorRGBA::ColorRGBA(void) : r(0), g(0), b(0), a(255) {}
    ColorRGBA::ColorRGBA(const uint8_t r, const uint8_t g, const uint8_t b, const uint8_t a) : r(r), g(g), b(b), a(a) {}
    ColorRGBA::ColorRGBA(const ColorRGB&  clr) : r(clr.r), g(clr.g), b(clr.b), a(255) {}
    ColorRGBA::ColorRGBA(const ColorRGBA& clr) : r(clr.r), g(clr.g), b(clr.b), a(clr.a) {}

    ColorRGBA::operator ColorRGB() const { return ColorRGB(r, g, b); }
}