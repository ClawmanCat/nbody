// Copyright (c) Rob Klein Ikink, 2018. Licensed under CC-BY 4.0
// http://creativecommons.org/licenses/by/4.0/deed.en_US

#include "Engine.h"
#include "Event.h"
#include "InputEvent.h"
#include "World.h"
#include "WorldPhysics.h"

#include <SDL/SDL.h>
#include <GLEW/glew.h>

#include <iostream>
#include <algorithm>

namespace nbody {
    const std::string Engine::PATH_CONFIGURATION                                                                                            = "./json/";
    Configuration Engine::CONFIG                                                                                                            = Configuration("universum", true);

    double Engine::dt                                                                                                                       = Engine::CONFIG.getValue<double>("tijdsstap_sec",   100);
    double Engine::tpf                                                                                                                      = Engine::CONFIG.getValue<double>("ticks_per_frame", 100);

    Universe Engine::universe                                                                                                               = Universe();
    UniverseRenderer Engine::renderer                                                                                                       = UniverseRenderer();
    
    EventDispatcher Engine::EVENT_DISPATCHER                                                                                                = EventDispatcher();
    WindowManager Engine::WINDOW                                                                                                            = WindowManager();
    InputManager Engine::INPUT                                                                                                              = InputManager();
    Camera Engine::CAMERA                                                                                                                   = Camera();

    void Engine::run(int32_t argc, char** argv) {
        SDL_Init(SDL_INIT_EVERYTHING);

        WINDOW.createWindow("NBody", 0, 0, 1920, 1080, WindowManager::WINDOWED);
        SDL_MaximizeWindow(WINDOW.getWindowHandle());

        SDL_GLContext context = SDL_GL_CreateContext(WINDOW.getWindowHandle());

        glewInit();

        SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, GL_TRUE);
        glClearColor(0.0F, 0.0F, 0.0F, 1.0F);

        EVENT_DISPATCHER.addListener<WindowChangedEvent>([](Event* evnt){
            WindowChangedEvent* wEvent = (WindowChangedEvent*) evnt;
            if (wEvent->evnt == InputManager::WindowEvent::EXIT) Engine::exit();
        });

        EVENT_DISPATCHER.addListener<KeyEvent>([](Event* evnt) {
            KeyEvent* kEvent = (KeyEvent*) evnt;

            if (kEvent->key == SDLK_F10) WINDOW.setWindowMode(WindowManager::WINDOWED);
            if (kEvent->key == SDLK_F11) WINDOW.setWindowMode(WindowManager::FULLSCREEN);
        });

        std::cout << "tijdsstap: " << dt << "s" << std::endl;
        std::cout << "gravitatieconstante: " << WorldConstants::GRAVITATIONAL_CONSTANT << "m^3 kg^-1 s^-2" << std::endl;
        std::cout << "systeem: " << Engine::CONFIG.getValue<std::string>("naam_universum_defs") << std::endl;
        std::cout << std::endl;

        Configuration worldConfig = Configuration(Engine::CONFIG.getValue<std::string>("naam_universum_defs"), true);
        std::vector<World*> worlds = {};
        std::vector<World*> fakes = {};

        for (auto& tree : worldConfig.treeView()) {
            std::string name = tree.first;
            bool isFake = tree.second.get<bool>("flag_geen_echt_lichaam", false);

            double mass = tree.second.get<double>("massa");
            double rad  = tree.second.get<double>("straal");

            ColorRGB clr = {
                tree.second.get<uint8_t>("kleur.r"),
                tree.second.get<uint8_t>("kleur.g"),
                tree.second.get<uint8_t>("kleur.b")
            };

            if (tree.second.get_child_optional("locatie_crt")) {
                // Cartesian coordinates
                auto node = tree.second.get_child("locatie_crt");

                std::string parent = node.get<std::string>("ouder");

                Position3<double> pos = {
                    node.get<double>("px"),
                    node.get<double>("py"),
                    node.get<double>("pz")
                };

                Position3<double> vel = {
                    node.get<double>("vx"),
                    node.get<double>("vy"),
                    node.get<double>("vz")
                };

                if (parent != "Oorsprong") {
                    for (World* world : worlds) {
                        if (world->getName() == parent) {
                            pos += world->getPosition();
                            vel += world->getVelocity();
                            break;
                        }
                    }
                }

                World* world = new World(boost::uuids::random_generator()(), name, mass, rad, clr, pos, vel);

                worlds.push_back(world);
                if (isFake) fakes.push_back(world);
            } else {
                // Kepler Orbit
                auto node = tree.second.get_child("locatie_kpl");

                std::string parent = node.get<std::string>("ouder");

                double semimajor_axis       = node.get<double>("halve_lange_as");
                double eccentricity         = node.get<double>("excentriciteit");
                double inclination          = node.get<double>("inclinatie");
                double asc_node             = node.get<double>("klimmende_knoop");
                double arg_periapsis        = node.get<double>("argument_periapsis");
                double true_anomaly         = node.get<double>("ware_anomalie");

                double pMass = 0.0;

                World* parentWorld = nullptr;
                if (parent != "Oorsprong") {
                    for (World* world : worlds) {
                        if (world->getName() == parent) {
                            parentWorld = world;
                            pMass = world->getMass();
                            break;
                        }
                    }
                }

                auto cart = WorldPhysics::convertKeplerOrbit(pMass, semimajor_axis, eccentricity, inclination, asc_node, arg_periapsis, true_anomaly);

                cart.first += parentWorld->getPosition();
                cart.second += parentWorld->getVelocity();

                World* world = new World(boost::uuids::random_generator()(), name, mass, rad, clr, cart.first, cart.second);

                worlds.push_back(world);
                if (isFake) fakes.push_back(world);
            }
        }

        for (World* fake : fakes) {
            for (int32_t i = worlds.size() - 1; i >= 0; i--) {
                if (worlds[i]->getName() == fake->getName()) worlds.erase(worlds.begin() + i);
            }
        }

		World *sun = nullptr, *earth = nullptr, *moon = nullptr;
		World *target = nullptr;
        for (World* world : worlds) {
			if (world->getName() == Engine::CONFIG.getValue<std::string>("focus")) target = world;
			if (world->getName() == "Zon"  ) sun   = world;
			if (world->getName() == "Aarde") earth = world;
			if (world->getName() == "Maan" ) moon  = world;

            Engine::universe.addWorld(world);
            Engine::renderer.addObject(world);
        }

        Engine::EVENT_DISPATCHER.addListener<KeyEvent>([&](Event* evnt){
            KeyEvent* kEvent = (KeyEvent*) evnt;

            if (kEvent->key == SDLK_MINUS) {
                Engine::dt *= 0.5;
                std::cout << "Tijdsstap is nu " << Engine::dt << " seconden/tick" << std::endl;
            }

            if (kEvent->key == SDLK_EQUALS) {
                Engine::dt *= 2.0;
                std::cout << "Tijdsstap is nu " << Engine::dt << " seconden/tick" << std::endl;
            }

            if (kEvent->key == SDLK_LEFTBRACKET) {
                Engine::tpf = std::max(Engine::tpf * 0.5, 1.0);
                std::cout << "Ticks per frame is nu " << Engine::tpf << " ticks/frame" << std::endl;
            }

            if (kEvent->key == SDLK_RIGHTBRACKET) {
                Engine::tpf *= 2.0;
                std::cout << "Ticks per frame is nu " << Engine::tpf << " ticks/frame" << std::endl;
            }

			if (kEvent->key == SDLK_n) {
				CAMERA.zoom(0.5);
			}

			if (kEvent->key == SDLK_m) {
				CAMERA.zoom(2.0);
			}
        });

		CAMERA.zoom(CONFIG.getValue<float>("zoom"));

        double ap = 0;
        double time = 0;
        bool last = false;
        uint64_t tick = 0;
        while (true) {
            //if (tick % 1'000 == 0) std::cout << "tick=" << tick << ", tijd=" << time / 60 / 60 / 24 << "d" << std::endl; 

            INPUT.update();

            if (INPUT.getMouseWheelCurrent() != INPUT.getMouseWheelPrevTick()) {
                double d = (INPUT.getMouseWheelCurrent() - INPUT.getMouseWheelPrevTick() > 0) ? 2.0 : 0.5;
                CAMERA.zoom(d);
            }

			if (Engine::CONFIG.getValue<bool>("voorspellingen")) {
				if (WorldPhysics::doesBodyOcclude(sun, earth, moon)) {
					if (!last) std::cout << std::setprecision(32) << "Eclipse @ J2000 + " << time << "seconds" << std::endl;
					last = true;
				}
				else {
					last = false;
				}
			}

			static Position3<double> old = { 0.0, 0.0, 0.0 };
			if (target != nullptr) {
				auto dpos = target->getPosition() - old;
				old = target->getPosition();

				CAMERA.setPosition(CAMERA.getPosition() + dpos);
			}

            Engine::universe.tick();
            time += dt;

            if (tick % (int) tpf == 0) {
                glClearDepth(1.0);
                glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

                Engine::renderer.render();

                SDL_GL_SwapWindow(WINDOW.getWindowHandle());
            }

            tick++;
        }
    }

    void Engine::exit(void) {
        SDL_Quit();
        std::exit(-1);
    }

    
}