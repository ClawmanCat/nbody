// Copyright (c) Rob Klein Ikink, 2018. Licensed under CC-BY 4.0
// http://creativecommons.org/licenses/by/4.0/deed.en_US

#pragma once

#include "Event.h"

#include <SDL/SDL.h>

#include <string>
#include <cstdint>
#include <utility>

// TODO: Redo this, removing member variables, instead using the SDL functions directly.
// TODO: Stop window from resizing when it's moved.
namespace nbody {
    class WindowManager {
    public:
        enum WindowMode { WINDOWED, BORDERLESS, FULLSCREEN };

        const static int32_t WINDOW_CENTERED = -1;
        const static int32_t WINDOW_FIT_TO_SCREEN = -2;

        WindowManager(void);
        ~WindowManager(void);

        void createWindow(std::string title, int32_t x, int32_t y, int32_t w, int32_t h, WindowMode mode);
        void destroyWindow(void);

        void changeWindow(std::string title, int32_t x, int32_t y, int32_t w, int32_t h, WindowMode mode);

        void setWindowMode(WindowMode mode) {
            auto pos  = getWindowPosition();
            auto size = getWindowSize();

            this->changeWindow(getWindowTitle(), pos.first, pos.second, size.first, size.second, mode);
        }

        std::pair<int32_t, int32_t> getScreenSize(int32_t screenID);

        std::pair<int32_t, int32_t> getWindowPosition(void);
        std::pair<int32_t, int32_t> getWindowSize(void);
        std::string                 getWindowTitle(void);
        WindowMode                  getWindowMode(void);
        SDL_Window*                 getWindowHandle(void);
    private:
        void onExternalWindowUpdate(Event* evnt);

        SDL_Window* handle;
        bool isInitialized;
    };
}