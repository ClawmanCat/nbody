#include "Universe.h"
#include "Engine.h"

namespace nbody {
void Universe::tick(void) {
        for (World* world : worlds) {
            if (rmQueue.find(world) == rmQueue.end()) {
                world->tick_gravity(this->worlds, Engine::dt);
                world->tick_movement(this->worlds, Engine::dt);
                world->tick_collision(this->worlds, Engine::dt);
            }
        }

        for (World* world : rmQueue) {
            auto it = this->worlds.find(world);
            if (it != this->worlds.end()) {
                std::cout << "Wereld " << world->getName() << " is verwijderd." << std::endl;
                this->worlds.erase(world);
            }
        }
        rmQueue.clear();

        for (World* world : addQueue) {
            std::cout << "Wereld " << world->getName() << " is geplaatst op " << world->getPosition().toString() << "(" << toStr(MathUtils::magnitude(world->getPosition())) << ")" << " met v=" << toStr(MathUtils::magnitude(world->getVelocity())) << std::endl;
            this->worlds.insert(world);
        }
        addQueue.clear();
    }
}