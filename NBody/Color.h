// Copyright (c) Rob Klein Ikink, 2018. Licensed under CC-BY 4.0
// http://creativecommons.org/licenses/by/4.0/deed.en_US

#pragma once

#include <cstdint>

namespace nbody {
    struct ColorRGBA;

    struct ColorRGB {
        uint8_t r, g, b;

        ColorRGB(void);
        ColorRGB(const uint8_t r, const uint8_t g, const uint8_t b);
        ColorRGB(const ColorRGB&  clr);
        ColorRGB(const ColorRGBA& clr);

        operator ColorRGBA() const;
    };

    struct ColorRGBA {
        uint8_t r, g, b, a;

        ColorRGBA(void);
        ColorRGBA(const uint8_t r, const uint8_t g, const uint8_t b, const uint8_t a);
        ColorRGBA(const ColorRGB&  clr);
        ColorRGBA(const ColorRGBA& clr);

        operator ColorRGB() const;
    };
}