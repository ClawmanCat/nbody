// Copyright (c) Rob Klein Ikink, 2018. Licensed under CC-BY 4.0
// http://creativecommons.org/licenses/by/4.0/deed.en_US

#pragma once

#include "Position3.h"
#include "Utility.h"
#include "Color.h"

#include <boost/uuid/uuid.hpp>

#include <string>
#include <cstdint>
#include <utility>
#include <unordered_set>

namespace nbody {
    class World {
    public:
        World(
            boost::uuids::uuid id,
            std::string name,
            double mass,
            double radius,
            ColorRGB mapTraceColor,
            Position3<double> position,
            Position3<double> velocity,
            bool isMovable = true,
            bool hasGravity = true
        );

        virtual ~World(void) {}

        // Simulate this world for a specified amount of time in seconds. (dt)
        virtual void tick_gravity(std::unordered_set<World*> others, float dt);
        virtual void tick_movement(std::unordered_set<World*> others, float dt);
        virtual void tick_collision(std::unordered_set<World*> others, float dt);

        void setVelocity(const Position3<double>& vel);

        // Automatically generated Getters/Setters
        MAKEGETSET(std::string, name, Name)
        MAKEGETSET(ColorRGB, mapTraceColor, MapTraceColor)
        MAKEGETSET(Position3<double>, position, Position)
        MAKEGETSET(double, bodyRadius, BodyRadius)
        MAKEGETSET(bool, isMovable, IsMovable)
        MAKEGETSET(bool, hasGravity, HasGravity)
        MAKEGETSET(double, bodyMass, Mass)

        MAKEGET(boost::uuids::uuid, id, ID)
        MAKEGET(Position3<double>, velocity, Velocity)
        MAKEGET(double, bodyMass, BodyMass)
    private:
        // The world's ID is guaranteed to be unique. The name is not.
        std::string name;
        boost::uuids::uuid id;

        // The world's trace color in the map view.
        ColorRGB mapTraceColor;

        // The object's position and velocity. (in meters and meters per second respectively)
        Position3<double>  position, velocity; 


        double bodyMass;                    // The mass of this object. (kg)

        double bodyRadius;                  // The radius of this body. Other worlds closer than this range will collide with this world. (m)

        bool isMovable;                     // Whether or not this world's velocity can be changed.
        bool hasGravity;                    // Whether or not this world will be included when calculating gravity.
    };
}