// Copyright (c) Rob Klein Ikink, 2018. Licensed under CC-BY 4.0
// http://creativecommons.org/licenses/by/4.0/deed.en_US

#pragma once

#include "Utility.h"

#include <SDL/SDL.h>

#include <unordered_map>
#include <string>
#include <cstdint>
#include <utility>

namespace nbody {
    class InputManager {
    public:
        enum MouseButton { LEFT, RIGHT, MIDDLE, X1, X2, MWHEEL_UP, MWHEEL_DOWN };
        enum WindowEvent { EXIT, MOVED, RESIZED, FOCUSSED };

        // A KeyBinding can be mapped to either a key on the keyboard or an action of the mouse.
        struct KeyBinding {
            KeyBinding(SDL_Keycode key) : type(KEYBOARD) {
                value.key = key;
            }

            KeyBinding(MouseButton btn) : type(MOUSE) {
                value.mouse = btn;
            }

            enum ValueType { KEYBOARD, MOUSE } type;

            union {
                SDL_Keycode key;
                MouseButton mouse;
            } value;
        };

        // A MouseEvent stores the actions of the mouse since a certain key was pressed.
        struct MouseEvent {
            std::pair<int32_t, int32_t> mouseBegin, mouseCurrent;       // The begin and current position of the mouse.
            int32_t mwheelBegin, mwheelCurrent;                         // The begin and current rotation of the mousewheel.
            MouseButton button;                                         // Which mouse button was pressed.
            bool hasEnded;                                              // Whether or not the mouse button has been released.

            bool operator==(const MouseEvent& evnt) const { return this->button == evnt.button; }
            bool operator!=(const MouseEvent& evnt) const { return this->button != evnt.button; }
            bool operator< (const MouseEvent& evnt) const { return this->button <  evnt.button; }
            bool operator> (const MouseEvent& evnt) const { return this->button >  evnt.button; }
            bool operator<=(const MouseEvent& evnt) const { return this->button <= evnt.button; }
            bool operator>=(const MouseEvent& evnt) const { return this->button >= evnt.button; }
        };

        void update(void);

        void registerKeybinding(std::string name, KeyBinding key);

        bool isKeyPressed(SDL_Keycode key);
        bool isMousePressed(MouseButton button);
        bool hasWindowChanged(WindowEvent evnt);

        bool isKeyBindingPressed(std::string bind);

        void markKnown(WindowEvent evnt);

        MAKEGETSET(int32_t, mwheelCurrent, MouseWheelCurrent)
        MAKEGETSET(int32_t, mwheelPrevTick, MouseWheelPrevTick)
    private:
        std::unordered_map<std::string, KeyBinding>     keybindings;

        std::unordered_map<SDL_Keycode, bool>           keyMap;
        std::unordered_map<MouseButton, MouseEvent>     mouseMap;
        std::unordered_map<WindowEvent, bool>           windowMap;

        std::pair<uint32_t, uint32_t> mouseCurrent, mousePrevTick;
        int32_t mwheelCurrent, mwheelPrevTick;
    };
}