// Copyright (c) Rob Klein Ikink, 2018. Licensed under CC-BY 4.0
// http://creativecommons.org/licenses/by/4.0/deed.en_US

#pragma once

/* Some compilers (namely MSVC) do not support boost's float128 type,
* since it is a wrapper around built-in types, which may or may not exist.
* This wrapper will automatically check support for float128, and use a
* software-based implementation instead if there is none.
* However, this alternative implementation is very slow, and should not be used
* when compiling for release. (This wrapper will check for that too!) */

// Define NO_BOOST_F128 if boost::multiprecision::float128 is not supported.
#if !defined(BOOST_MP_USE_FLOAT128) && !defined(BOOST_MP_USE_QUAD)
    #ifndef NO_BOOST_F128
        #define NO_BOOST_F128
    #endif
#endif

// Include the proper header.
#ifdef NO_BOOST_F128
    #include <boost/multiprecision/cpp_bin_float.hpp>
    typedef boost::multiprecision::cpp_bin_float_100 float128;
#else
    #include <boost/multiprecision/float128.hpp>
    typedef boost::multiprecision::float128 float128;
#endif

// Don't compile for release with cpp_bin_float, since it's a lot slower.
#if !defined(_DEBUG) && defined(NO_BOOST_F128)
    #error Don't compile for release with cpp_bin_float!
#endif