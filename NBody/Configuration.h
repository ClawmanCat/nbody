// Copyright (c) Rob Klein Ikink, 2018. Licensed under CC-BY 4.0
// http://creativecommons.org/licenses/by/4.0/deed.en_US

#pragma once

#include <boost/property_tree/ptree.hpp>

#include <string>

namespace nbody {
    class Configuration {
    public:
        Configuration(std::string name, bool readOnCreation) : name(name) {
            if (readOnCreation) readConfiguration();
        }

        template <class T> T getValue(std::string key) {
            return tree.get<T>(key);
        }

        template <class T> T getValue(std::string key, const T& defaultValue) {
            return tree.get<T>(key, defaultValue);
        }

        template <class T> void setValue(std::string key, T& val) {
            tree.set(key, val);
        }

        boost::property_tree::ptree& treeView(void) {
            return tree;
        }

        void readConfiguration(void);
        void writeConfiguration(void);
    private:
        std::string name;
        boost::property_tree::ptree tree;
    };
}