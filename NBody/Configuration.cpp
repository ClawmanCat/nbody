// Copyright (c) Rob Klein Ikink, 2018. Licensed under CC-BY 4.0
// http://creativecommons.org/licenses/by/4.0/deed.en_US

#include "Configuration.h"
#include "Engine.h"
#include "Exception.h"

#include <boost/property_tree/json_parser.hpp>
#include <boost/exception/all.hpp>

#include <iostream>

namespace nbody {
    void Configuration::readConfiguration(void) {
        try {
            tree.clear();
            boost::property_tree::read_json(Engine::PATH_CONFIGURATION + name + ".json", tree);
        } catch (const boost::property_tree::json_parser_error& e) {
            throw new IOException("Failed to read configuration file: " + Engine::PATH_CONFIGURATION + name + ".json");
        }
    }

    void Configuration::writeConfiguration(void) {
        try {
            boost::property_tree::write_json(Engine::PATH_CONFIGURATION + name + ".json", tree);
        } catch (...) {
            throw new IOException("Failed to write configuration file: " + Engine::PATH_CONFIGURATION + name + ".json");
        }
    }
}